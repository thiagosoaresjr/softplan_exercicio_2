package com.softplan.exercicio;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utilitáro para criação de observações
 * 
 * @author thiago.soares
 *
 */
public class GeradorObservacao {

  // Template da observação no singular
  private static final String TEMPLATE_OBSERVACAO_SINGULAR = "Fatura da nota fiscal de simples remessa: ";
  private static final String TEMPLATE_OBSERVACAO_PLURAL   = "Fatura das notas fiscais de simples remessa: ";
  

  /**
   * Gerar observações.
   * Os números das notas fiscais recebidos como parâmetro são contatenados em templates de
   * observações pré-definidas
   * 
   * @param lista Lista de codigos de notas fiscais
   * @return Observacao
   */
  public String gerarObservacao(List<Integer> lista) {
    StringBuffer textoConcatenado = new StringBuffer();
    if (lista != null && !lista.isEmpty()) {
    	
      lista = cleanNullItems(lista);

      textoConcatenado.append(definirTemplate(lista));

      textoConcatenado.append(montarListaNotasFiscais(lista));

      textoConcatenado.append(".");

      return textoConcatenado.toString();
    }
    return "";
  }

  /**
   * Define o template de mensagem a ser utilizado
   * 
   * @param lista Lista de codigos de notas fiscais
   * @return Observacao
   */
  private String definirTemplate(List<Integer> lista) {

    if(lista == null) {
      return "";
    }

    if (lista.size() >= 2) {
      return TEMPLATE_OBSERVACAO_PLURAL;
    } else {
      return TEMPLATE_OBSERVACAO_SINGULAR;
    }
  }

  /**
   * Utilitário interno para criação da observação.
   * Monta a lista de notas fiscais e concatena com o template da observação
   * 
   * @param lista Lista de codigos de notas fiscais
   * @return String Lista de codigos de notas fiscais
   */
  private String montarListaNotasFiscais(List<Integer> lista) {
	  
	  if(lista.size() == 1) {
		  return lista.get(0).toString();
	  } else {
		  
		  return lista.stream()
				  .map( i -> i.toString())  // converter o List<Integer> para List<String>
				  .limit(lista.size() - 1)  // Limitar a primeira parte da lista, que receberá vírgula
				  .collect(Collectors.joining(", ", "", " e "))    // Contatenar as vírgulas e o "e" final
				  .concat(lista.get(lista.size() - 1).toString()); // Concatenar o último item, após o "e"  
		  
	  }
	  
  }
  
  private List<Integer> cleanNullItems(List<Integer> lista) {
	  return lista.stream().filter( item -> item != null).collect(Collectors.toList());
  }
}
